#include <stdio.h>

int main()
{
	float angle = 0, angleOut = 0;
	char mode = 'D';
	printf("Please enter value of angle\n");
	scanf("%f%c", &angle, &mode);
	if (mode == 'D')
	{
		angleOut = angle * 3.14 / 180;
		printf("You have entered value in degrees\n");
		printf("Value in radian equals to: %f\n", angleOut);
	}
	else if (mode == 'R')
	{
		angleOut = angle * 180 / 3.14;
		printf("You have entered value in radian\n");
		printf("Value in degrees equals to: %f\n", angleOut);
	}
	return 0;
}