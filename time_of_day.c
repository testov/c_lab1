#include <stdio.h>

int main()
{
	int hour = 0, minute = 0, second = 0;
	printf("Please enter current time\n");
	scanf("%d:%d:%d", &hour, &minute, &second);
	if (hour<0 || hour>23 || minute<0 || minute>59 || second<0 || second>59)
		printf("Youe entered invalid time\n");
	else if (hour<7 || hour>22)
		printf("Good night\n");
	else
		printf("Good time for a great deals!\n");
	return 0;
}