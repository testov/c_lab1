#include <stdio.h>
#include <string.h>
#define ScreenWidth 80

int main()
{
	unsigned char buf[ScreenWidth];
	int len = 0, indent = 0, Ind;
	puts("Enter a string:");
	fgets(buf,ScreenWidth,stdin);
	len = strlen(buf);
	indent = (ScreenWidth - len)/2;
	for (Ind=0;Ind<indent;Ind++)
		putchar(' ');
	puts(buf);
	return 0;
}